const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

let productionPlugins = [];
// eslint-disable-next-line no-process-env
if (process.env.NODE_ENV === 'production') {
    productionPlugins = [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new webpack.DefinePlugin({
            // eslint-disable-next-line no-process-env
            'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV) }
        })
    ];
}

module.exports = {
    context: __dirname,
    entry: [
        './src/index.js'
    ],
    output: {
        path: path.resolve('./dist'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    module: {
        preLoaders: [
            {
                test: /\.jsx?$/,
                loader: 'eslint-loader',
                exclude: /node_modules/
            }
        ],
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel'
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('style-loader', 'css-loader?importLoaders=1', 'postcss-loader')
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/font-woff'
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=application/octet-stream'
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file'
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url?limit=10000&mimetype=image/svg+xml'
            }
        ]
    },
    devtool: '#source-map',
    eslint: {
        configFile: '.eslintrc.json'
    },
    postcss: [ 'autoprefixer' ],
    plugins: [
        new webpack.optimize.OccurenceOrderPlugin(),
        new ExtractTextPlugin('main.css', { allChunks: true }),
        // new webpack.NoErrorsPlugin(),
        ...productionPlugins
    ]
};
