import React, { PropTypes } from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';

import MainNav from '../styled_components/main-nav';
import ReviewBlock from '../styled_components/review-block';

const styles = StyleSheet.create({
    hero: {
        position: 'relative',
        minHeight: '45rem',
        overflow: 'hidden',
        zIndex: 1
    },
    content: {
        position: 'relative',
        backgroundColor: '#f5f6f7',
        width: '80%',
        margin: '-10rem auto',
        zIndex: 2
    },
    wrapper: {
        width: '100%'
    },
    reviews: {
        position: 'relative',
        top: '-10rem',
        margin: '0 auto',
        width: '80%'
    }
});

const reviews = ({ heroBackgroundUrl, items }) => {
    const style = StyleSheet.create({
        heroBackroung: {
            backgroundImage: `url(${heroBackgroundUrl})`
        }
    });
    return (
        <div>
            <header className={css(styles.hero, style.heroBackroung)}>
                <MainNav />
            </header>
            <section className={css(styles.content)}>
                <div className={css(styles.wrapper)}>
                    <div className={css(styles.reviews)}>
                        {
                            items.map((item) => <ReviewBlock key={item.id} {...item} />)
                        }
                    </div>
                </div>
            </section>
        </div>
    );
};

// to do : add rest of the fields
reviews.propTypes = {
    heroBackgroundUrl: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.shape({
    }))
};
reviews.defaultProps = {
    heroBackgroundUrl: '',
    items: []
};

export default reviews;
