import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';

import Rating from './rating';
import Avatar from './avatar';
import Comment from './comment-block';

const styles = StyleSheet.create({
    box: {
        backgroundColor: '#fff',
        padding: '1.5em',
        marginBottom: '6em',
        boxShadow: '0 0 2em #6d6f73'
    },
    title: {
        textAlign: 'center'
    },
    review: {
        textAlign: 'justify',
        padding: '0 2em'
    },
    button: {
        display: 'block',
        margin: '0 auto',
        backgroundColor: '#fad73c',
        border: '1px solid #fad73c',
        fontWeight: 'bold',
        color: '#2d6f8f',
        textTransform: 'uppercase',
        padding: '1.25em 4.5em',

        ':hover': {
            cursor: 'pointer',
            backgroundColor: 'transparent',
            transition: 'all .5s'
        },
        ':focus': {
            outline: 'none'
        }
    },
    avatar: {
        marginTop: '-4em',
        textAlign: 'center'
    }
});

const mapComments = (comment) =>
    <Comment
        author={comment.author}
        key={comment.id}
    >{comment.content}</Comment>;

// eslint-disable-next-line react/no-multi-comp
export default function ReviewBlock({ title, content, rating, author, comments }) {
    const commentSection = Array.isArray(comments) && comments.length > 0 ?
        comments.map(mapComments) : <button className={css(styles.button)}>Add comment</button>;

    return (
        <div className={css(styles.box)}>
            <Avatar
                caption={author.timestamp}
                className={css(styles.avatar)}
                name={author.name}
                size={100}
                url={author.avatarUrl}
            />
            <h3 className={css(styles.title)}>{title}</h3>
            <Rating value={rating} />
            <p className={css(styles.review)}>
                {content}
            </p>
            {commentSection}
        </div>
    );
}

ReviewBlock.propTypes = {
    author: PropTypes.shape(),
    comments: PropTypes.arrayOf(PropTypes.shape()),
    content: PropTypes.string,
    rating: PropTypes.number,
    title: PropTypes.string
};
