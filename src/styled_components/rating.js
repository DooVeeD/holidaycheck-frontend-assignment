import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';

const stars = [ ...Array(6) ];

const TEXT_PADDING = '.5em 1.5em';

const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        border: '1px solid #eff1f3',
        fontSize: '1.125em',
        fontWeight: 'bold',
        width: '80%',
        margin: '0 auto'
    },
    thumb: {
        display: 'inline-block',
        padding: TEXT_PADDING,
        color: '#fff'
    },
    thumbRed: {
        backgroundColor: '#e8402d'
    },
    thumbGreen: {
        backgroundColor: '#54b13f'
    },
    note: {
        padding: TEXT_PADDING
    },
    stars: {
        padding: TEXT_PADDING,
        textAlign: 'center',
        flexGrow: 99
    },
    star: {
        margin: '0.25em'
    },
    starShine: {
        color: '#fad73c'
    },
    starDimm: {
        color: '#dfe2e6'
    }
});

// this is a local component, extracted for readability
function Star({ isShining }) {
    return <i className={`fa fa-star ${css(styles.star, isShining ? styles.starShine : styles.starDimm)}`} />;
}
Star.propTypes = {
    isShining: PropTypes.bool.isRequired
};

// eslint-disable-next-line react/no-multi-comp
export default function Rating({ value }) {
    let thumbColor = styles.thumbGreen;
    let thumbDirection = 'up';
    if (value <= 3) {
        thumbColor = styles.thumbRed;
        thumbDirection = 'down';
    }

    return (
        <div className={css(styles.wrapper)}>
            <section className={css(styles.thumb, thumbColor)}>
                <i className={`fa fa-thumbs-${thumbDirection}`} />
            </section>
            <section className={css(styles.stars)}>
                { stars.map((_, i) => <Star isShining={value > i} key={i} />) }
            </section>
            <section className={css(styles.note)}>{ value } / 6</section>
        </div>
    );
}

Rating.propTypes = {
    value: PropTypes.number.isRequired
};
