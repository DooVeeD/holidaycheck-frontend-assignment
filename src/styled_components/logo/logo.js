import './logo.css';
import React, { PropTypes } from 'react';

import SVGLogo from 'babel!svg-react!../../assets/hc-logo.svg?=name=logo';

export default function Logo(props) {
    return <SVGLogo width="200px" {...props} />;
}

Logo.propTypes = {
    className: PropTypes.string
};
