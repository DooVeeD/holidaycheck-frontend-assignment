import React from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';

import Logo from './logo/logo.js';

const styles = StyleSheet.create({
    nav: {
        backgroundColor: '#fff',
        width: '80%',
        height: '8rem',
        margin: '2em auto',
        boxShadow: '0 0 2em #6d6f73'
    },
    logo: {
        padding: '1em'
    },
    links: {
        float: 'right',
        margin: '0 1em'
    },
    link: {
        padding: '2.5em 2em',
        display: 'inline-block',
        boxSizing: 'border-box'
    },
    selected: {
        backgroundColor: '#f5f6f7'
    }
});

const MainNav = () => <nav className={css(styles.nav)}>
    <Logo className={css(styles.logo)} />
    <section className={css(styles.links)}>
        <a className={css(styles.link)} href="#">Dashboard</a>
        <a className={css(styles.link, styles.selected)} href="#">Reviews</a>
        <a className={css(styles.link)} href="#">Hotel Manager</a>
        <a className={css(styles.link)} href="#">Settings</a>
    </section>
</nav>;

export default MainNav;
