import React, { PropTypes } from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';

export default function Avatar({ className, url, name, caption, size }) {
    const s = `${size}px`;
    const style = StyleSheet.create({
        pic: {
            borderRadius: `${size / 2}px`
        }
    });
    return (
        <div className={className}>
            <img
                alt={`Picture of ${name}`}
                className={css(style.pic)}
                height={s}
                src={url}
                width={s}
            /><br />
            <span>{name}</span><br />
            <small>{caption}</small>
        </div>
    );
}

Avatar.propTypes = {
    caption: PropTypes.string,
    className: PropTypes.string,
    name: PropTypes.string,
    size: PropTypes.number,
    url: PropTypes.string
};
