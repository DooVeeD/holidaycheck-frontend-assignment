import React, { PropTypes } from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';

import Avatar from './avatar';

const styles = StyleSheet.create({
    comment: {
        marginTop: '2em',
        position: 'relative',
        backgroundColor: '#edf0f2 ',
        padding: '1em 3em 3em',
        textAlign: 'justify',

        ':after': {
            bottom: '100%',
            left: '50%',
            border: 'solid transparent',
            content: '" "',
            height: 0,
            width: 0,
            position: 'absolute',
            pointerEvents: 'none',
            borderColor: 'rgba(237, 240, 242, 0)',
            borderBottomColor: '#edf0f2',
            borderWidth: '30px',
            marginLeft: '-30px'
        }
    },
    avatar: {
        position: 'relative',
        textAlign: 'center',
        marginTop: '-2.5em'
    }
});

export default function CommentBlock({ author, children }) {
    return (
        <div>
            <div className={css(styles.comment)}>
                {children}
            </div>
            <Avatar
                caption={author.role}
                className={css(styles.avatar)}
                name={author.name}
                size={100}
                url={author.avatarUrl}
            />
        </div>
    );
}

CommentBlock.propTypes = {
    author: PropTypes.shape(),
    children: PropTypes.string
};
