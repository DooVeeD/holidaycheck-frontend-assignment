import React, {Component, PropTypes} from 'react';
import { StyleSheet, css } from 'aphrodite/no-important';

const styles = StyleSheet.create({
    input: {
        minWidth: '20em',
        padding: '0.5em',
        borderWidth: '0 0 1px 0',
        color: '#6d6f73', // TODO move to colors
        fontWeight: 600,
        outline: 'none',
        backgroundColor: '#d9d9d9'
    }
});

export default class ControlledInput extends Component {
    constructor (props) {
        super(props);
        this.state = {
            value: props.initialValue
        };
    }

    handleChange (ev) {
        const value = ev.target.value;
        this.setState({value});
        this.props.onChangeHandler(value);
    }

    render () {
        const {value} = this.state;
        return (
            <input type="text" className={css(styles.input)} value={value} onChange={this.handleChange.bind(this)} />
        );
    }
};

ControlledInput.propTypes = {
    initialValue: PropTypes.string,
    onChangeHandler: PropTypes.func,
};

ControlledInput.defaultProps = {
    initialValue: '',
    onChangeHandler: () => {}
};
