import React, {Component, PropTypes} from 'react';

export default class ControlledCheckbox extends Component {
    constructor (props) {
        super(props);
        this.state = {
            value: props.initialValue
        };
    }
    componentWillReceiveProps (nextProps) {
        // We need to update state because component is already mounted and model changed it's state
        if (this.state.value !== nextProps.initialValue) {
            this.setState({value: nextProps.initialValue});
        }
    }
    handleChange (ev) {
        const value = ev.target.checked;
        this.setState({value});
        this.props.onChangeHandler(value);
    }

    render () {
        const {value} = this.state;
        return (
            <input type="checkbox" checked={value} onChange={this.handleChange.bind(this)} />
        );
    }
};

ControlledCheckbox.propTypes = {
    initialValue: PropTypes.bool,
    onChangeHandler: PropTypes.func,
};

ControlledCheckbox.defaultProps = {
    initialValue: false,
    onChangeHandler () {}
};
