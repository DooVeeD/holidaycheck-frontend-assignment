import React, { Component, PropTypes } from 'react';
import { css, StyleSheet } from 'aphrodite/no-important';
import equal from 'deep-equal';

function timeout(time) {
    return new Promise((res) => setTimeout(() => res(), time));
}

async function getData(url) {
    // add 2 sec deley for dev purpose
    await timeout(2000);
    const res = await fetch(url);
    const data = await res.json();
    return data;
}

const styles = StyleSheet.create({
    loader: {
        fontSize: '1.5em',
        textAlign: 'center',
        width: '100%',
        padding: '10em 0 0'
    }
});

export default class WithFetch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            data: null
        };
    }
    componentDidMount() {
        this.fetchData(this.props.url);
    }
    componentWillReceiveProps(props) {
        this.fetchData(props.url);
    }
    shouldComponentUpdate(nextProps, nextState) {
        if (this.props.url !== nextProps.url) {
            return true;
        }
        if (!equal(this.state.data, nextState.data)) {
            return true;
        }
        return false;
    }
    fetchData(url) {
        // eslint-disable-next-line react/no-set-state
        this.setState({ isLoading: true });
        getData(url).then((data) => {
            // eslint-disable-next-line react/no-set-state
            this.setState({ data, isLoading: false });
        });
    }
    render() {
        // for some reason eslint cannot decide and complains on extra parens and missing parens around multiline jsx
        // eslint-disable-next-line no-extra-parens
        const loadingIndicator = (
            <div className={css(styles.loader)}>
                <i className="fa fa-spinner fa-pulse fa-3x fa-fw" /><br />
                Loading...
            </div>
        );
        return this.state.isLoading ? loadingIndicator : this.props.children(this.state.data);
    }
}

WithFetch.propTypes = {
    children: PropTypes.func.isRequired,
    url: PropTypes.string.isRequired
};
