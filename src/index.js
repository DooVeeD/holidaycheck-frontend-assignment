import 'babel-polyfill';

import './styles/main.css';
import 'font-awesome/css/font-awesome.css';

import React from 'react';
import { render } from 'react-dom';

import FetchData from './containers/fetch-data';
import Reviews from './pages/reviews';

const App = () => <FetchData url="/data.mock.json">
    {
        ({ reviews, pictureUrl }) => <Reviews heroBackgroundUrl={pictureUrl} items={reviews} />
    }
</FetchData>;

render(<App />, document.querySelector('#app-container'));
