# Holidaycheck Front-End Assignment
### by Dawid Jankowiak

## Requirements
  * [Node.js](https://nodejs.org/) in version `6.9.0` or higher
  * npm in version `3.0.0` or higher (it will be installed with Node.js)
  * *(optionally)* [yarn](http://yarnpkg.com) in version `0.16.0`
  * evergreen web browser

To check if you have them installed open terminal (or command prompt) and type
`node -v`, `npm -v` and `yarn --version`.

## How to start
1. Open terminal in root folder and type `npm install` or if you use yarn just `yarn` would do the trick
2. Run `npm run build` to build production ready files
3. Run `npm start` to start web server, then go to `localhost:8080`

## How to develop
1. Run `npm i` or `yarn`
2. Run `npm run dev` to start development server
3. Go to `localhost:8080`
4. All changes to source files will automatically refresh page, changes to configuration files will require restarting server

## Author comments and thoughts
  * Because this is so small "application" ;) I've didn't use any state management or model library. If this would grow bigger I would introduce [Redux](http://redux.js.org/)
    and for dealing with server side calls I would consider [fetch](https://github.com/matthew-andrews/isomorphic-fetch) and [redux-thunk](https://github.com/gaearon/redux-thunk)
    or [redux-saga](http://yelouafi.github.io/redux-saga/)
  * I've dicided to hardcode user names and pictures to not complicate code. Technically I could use `fetch('https://randomuser.me/api/')` and
    `Promise.all([...])` but that would require to cluther `FetchData` component or to build new one. And futhermore in regular
    application user data would come in response with the rest of data.
  * Why not to use some CSS framework like bootstrap or foundation? To be honest I don't work much with CSS frameworks, because of
    that I don't remember their API and I would wast a lot of time to search for things, and at the end of the day it would still
    need to be customized for this design.
